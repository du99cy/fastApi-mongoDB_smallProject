from fastapi import FastAPI,Query,Form,UploadFile,File,Request
import uvicorn
from typing import Optional
from fastapi.middleware.cors import CORSMiddleware
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from pydantic import BaseModel
from fastapi.responses import FileResponse
import time
import asyncio
import logging
import os
import aiofiles
from fastapi.staticfiles import StaticFiles
app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
origins = [
    "http://localhost",
    "http://localhost:4200",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class Settings(BaseModel):
    authjwt_secret_key: str = "my_jwt_secret"
@AuthJWT.load_config
def get_config():
    return Settings()
@app.exception_handler(AuthJWTException)
def authjwt_exception_handler(request: Request, exc: AuthJWTException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"detail": exc.message}
    )
@app.get("/items/")
async def read_items(q: Optional[str] = Query(None, min_length=3, max_length=50,regex="^helloWorld$")):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results

@app.get("/items2/")
async def mulList(p:list[int]=Query([3,2])):
    return {"p":p}

@app.get("/items3/")
async def read_items(
    q: Optional[str] = Query(
        None,
        title="Query string",
        description="Query string for the items to search in the database that have a good match",
        min_length=3,
    )
):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results



@app.get("/items4/")
async def read_items(
    q: Optional[str] = Query(
        None,
        alias="item-query",
        title="Query string",
        description="Query string for the items to search in the database that have a good match",
        min_length=3,
        max_length=50,
        #regex="^fixedquery$",
        deprecated=False,
    )
):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results 


@app.post('/uploadFile/')
async def uploadFile(file:UploadFile = File(...)):
    path = 'static/'+file.filename
    async with aiofiles.open(path,'wb') as f:
        content = await file.read()
        await f.write(content)
    return {'path':'localhost:8000/'+path}

@app.get("/testA")
async def read_root():
    print('Started')
    await asyncio.sleep(10)
    print('Finished')
    return {"Hello": "World"}


if __name__ == "__main__":
    uvicorn.run('test:app',host = "127.0.0.1",port = 8000,reload = True)